# Position A to B (Patches)
Experiment 022 in Facebook AR Studio (v42).

Three methods of moving an object to tap's location: direct, using Animation patch and using ExpSmooth patch.

Everything is documented, open the Patch editor and read comments / see issues with the methods.

### Get in touch

Any questions? hello@alexeverything.net or find me in [Facebook AR Studio group](https://www.facebook.com/groups/ARStudioCreatorCommunity/)
